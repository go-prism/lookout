ARG GITLAB_PREFIX
FROM ${GITLAB_PREFIX}registry.gitlab.com/av1o/base-images/go-git:1.17 as BUILDER

RUN mkdir -p /home/somebody/go && \
    mkdir -p /home/somebody/.tmp
WORKDIR /home/somebody/go

ARG GOPRIVATE
ARG AUTO_DEVOPS_GO_COMPILE_FLAGS

# copy our code in
COPY --chown=somebody:0 . .

# build the binary
RUN TMPDIR=/home/somebody/.tmp CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
    go build -a -installsuffix cgo -ldflags '-extldflags "-static"' $(echo "${AUTO_DEVOPS_GO_COMPILE_FLAGS}" | tr -d '"' | tr -d "'" | sed -e 's/%20/ /g') -o lookout ./cmd/lookout
RUN TMPDIR=/home/somebody/.tmp CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
    go build -a -installsuffix cgo -ldflags '-extldflags "-static"' $(echo "${AUTO_DEVOPS_GO_COMPILE_FLAGS}" | tr -d '"' | tr -d "'" | sed -e 's/%20/ /g') -o lscm ./cmd/scm

FROM registry.gitlab.com/av1o/base-images/alpine-builder:3

# install SSH utilities
USER root
RUN apk add --no-cache openssh skopeo
USER somebody

RUN mkdir -p /home/somebody/
WORKDIR /home/somebody/

COPY --from=BUILDER --chown=somebody:0 /home/somebody/go/lookout ./
COPY --from=BUILDER --chown=somebody:0 /home/somebody/go/lscm ./

# set permissions
RUN chmod +x ./lookout && \
    chmod +x ./lscm