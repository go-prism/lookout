PKG=gitlab.com/go-prism/lookout
VERSION=$(shell cat version.txt)
GIT_COMMIT?=$(shell git rev-parse HEAD)
BUILD_DATE?=$(shell date -u +"%Y-%m-%dT%H:%M:%SZ")
LDFLAGS?="-X ${PKG}/pkg/internal.Version=${VERSION} \
	-X ${PKG}/pkg/internal.GitCommit=${GIT_COMMIT} \
	-X ${PKG}/pkg/internal.BuildDate=${BUILD_DATE}"

.PHONY: bin/lookout
bin/lookout:
	CGO_ENABLED=0 go build -ldflags ${LDFLAGS} -o bin/lookout ./cmd/lookout

run: bin/lookout
	bin/lookout

.PHONY: bin/lscm
bin/lscm:
	CGO_ENABLED=0 go build -o bin/lscm ./cmd/scm