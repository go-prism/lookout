# Lookout

> Lookout is brand new and in active development.
> You should not use it in production.

Lookout is a CLI tool designed to watch remote container images for new versions.
It runs in a scheduled CI job and opens an MR when changes have been detected.

## Input formats

Lookout currently supports the following formats:

* URL
* File *(recommended)*

### URL

```bash
lookout --type=url docker://quay.io/dexidp/dex
```

### File

The file format provides the ability to customise the output and behaviour of Lookout.

* `name` - the name of the key when output (e.g. `image_tag_MY_KEY`)
* `url` - the Skopeo URL to the image (without a tag)
* `regex` - optional regex to limit the tags scanned
* `sha` - outputs image SHA rather than tag

```yaml
images:
  - name: dex
    url: docker://quay.io/dexidp/dex
    regex: "^v\\d+.*" # only tags starting with v[0-9]
    sha: true
  - name: origin-console
    url: docker://quay.io/openshift/origin-console
    regex: "^4.9" # only tags starting with 4.9
    sha: false
```

```bash
lookout --input=file ./images.yaml
```

## Output formats

Lookout currently supports the following formats:

* Key value `--output=kv`
* Terraform `--output=tf`
* Only value `--output=vv`

### Key value

Lookout will return key-value mappings of images:

```text
my-image=docker.io/my-repo/my-image:v1.234
my-other-image=docker.io/my-repo/my-other-image:v2.345
```

### Terraform

Lookout will return mappings of images which can be used as a `*.tfvars` file for
the Terraform CLI.

All variables will have the `image_tag_` prefix.

```terraform
image_tag_my-image = "docker.io/my-repo/my-image:v1.234"
image_tag_my-other-image = "docker.io/my-repo/my-other-image:v2.345"
```

### Only value

Lookout will return the image tags:

```text
docker.io/my-repo/my-image:v1.234
docker.io/my-repo/my-other-image:v2.345
```