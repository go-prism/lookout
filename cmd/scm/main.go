/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package main

import (
	_ "embed"
	"github.com/namsral/flag"
	log "github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
)

//go:embed description.md
var mrDescription string

func main() {
	token := flag.String("gitlab-api-token", "", "gitLab token (can be job, deploy or personal access token)")
	baseURL := flag.String("ci-api-v4-url", "https://gitlab.com/api/v4", "gitLab API v4 root URL")
	projectID := flag.String("ci-project-id", "", "gitLab project ID")
	targetBranch := flag.String("ci-default-branch", "master", "target branch to merge into")
	sourceBranch := flag.String("source-branch", "", "source branch we're currently on")

	flag.Parse()

	if *token == "" {
		log.Fatal("gitlab-api-token is nil")
	}
	if *projectID == "" {
		log.Fatal("ci-project-id is nil")
	}
	if *sourceBranch == "" {
		log.Fatal("source-branch is nil")
	}

	// create the client
	log.Infof("setting up gitlab client to instance: '%s'", *baseURL)
	git, err := gitlab.NewClient(*token, gitlab.WithBaseURL(*baseURL))
	if err != nil {
		log.WithError(err).Fatal("failed to construct gitlab client")
		return
	}

	// open the MR
	log.Infof("attempting to create MR for project '%s' from '%s' -> '%s'", *projectID, *sourceBranch, *targetBranch)
	mr, _, err := git.MergeRequests.CreateMergeRequest(*projectID, &gitlab.CreateMergeRequestOptions{
		Title:              gitlab.String("chore: update container image versions"),
		Description:        gitlab.String(mrDescription),
		SourceBranch:       sourceBranch,
		TargetBranch:       targetBranch,
		Labels:             []string{"dependencies"},
		RemoveSourceBranch: gitlab.Bool(true),
		Squash:             gitlab.Bool(true),
		AllowCollaboration: gitlab.Bool(false),
	})
	if err != nil {
		log.WithError(err).Fatal("failed to create MR")
		return
	}
	log.Infof("created MR with iid: %d", mr.IID)
}
