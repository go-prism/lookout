module gitlab.com/go-prism/lookout

go 1.16

require (
	github.com/araddon/dateparse v0.0.0-20210429162001-6b43995a97de
	github.com/containers/skopeo v1.4.1
	github.com/namsral/flag v1.7.4-pre
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	github.com/urfave/cli/v2 v2.3.0
	github.com/xanzy/go-gitlab v0.50.4
	gopkg.in/yaml.v2 v2.4.0
)
