/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package app

import (
	"context"
	"errors"
	"fmt"
	"regexp"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	"gitlab.com/go-prism/lookout/pkg/config"
	"gitlab.com/go-prism/lookout/pkg/oci"
	"gitlab.com/go-prism/lookout/pkg/output"
)

var (
	ErrMissingArg   = errors.New("an argument is required")
	ErrInvalidImage = errors.New("image string is not a valid URL")
	RegexURL        = regexp.MustCompile(`[-a-zA-Z0-9@:%._+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_+.~#?&/=]*)`)
)

type Action struct {
	backendLoader func(mode string) oci.Inspector
}

func NewAction() *Action {
	a := new(Action)
	a.backendLoader = a.defaultBackendLoader

	return a
}

func (a *Action) defaultBackendLoader(mode string) oci.Inspector {
	switch mode {
	default:
		fallthrough
	case FlagBackendSkopeo:
		return &oci.SkopeoInspector{}
	}
}

func (a *Action) F(c *cli.Context) error {
	// check that we have our argument
	if c.NArg() < 1 {
		return ErrMissingArg
	}
	// fetch the inspector
	i := a.backendLoader(c.String(FlagBackend))
	log.Debugf("using inspector backend: %s", c.String(FlagBackend))
	// fetch the mode
	mode := c.String(FlagInput)
	log.Debugf("using mode: %s", mode)
	var images []*output.FormatPair
	switch mode {
	case FlagModeURL:
		img, err := a.getLatest(i, c.Args().Get(0), "")
		if err != nil {
			return err
		}
		images = []*output.FormatPair{
			{
				Name:   "default",
				Tag:    img.Tag,
				SHA:    img.Sha,
				Source: img.Source,
				UseSHA: false,
			},
		}
	case FlagModeFile:
		img, err := a.readInput(i, c.Args().Get(0))
		if err != nil {
			return err
		}
		images = img
	}

	var out output.Formatter
	switch c.String(FlagOutput) {
	default:
		fallthrough
	case FlagOutputKV:
		out = &output.KeyValue{}
	case FlagOutputTF:
		out = &output.Terraform{}
	case FlagOutputVV:
		out = &output.OnlyValue{}
	}
	log.WithContext(c.Context).Infof("using output formatter: %s", c.String(FlagOutput))
	str, err := out.Format(images)
	if err != nil {
		return err
	}
	fmt.Println(str)
	return nil
}

func (a *Action) readInput(ins oci.Inspector, path string) ([]*output.FormatPair, error) {
	c, err := config.ReadFile(path)
	if err != nil {
		return nil, err
	}
	var tags []*output.FormatPair
	for _, i := range c.Images {
		t, err := a.getLatest(ins, i.URL, i.Regex)
		if err != nil {
			continue
		}
		tags = append(tags, &output.FormatPair{
			Name:   i.Name,
			Tag:    t.Tag,
			SHA:    t.Sha,
			Source: t.Source,
			UseSHA: i.Sha,
		})
	}
	return tags, nil
}

func (*Action) getLatest(ins oci.Inspector, src, regex string) (*oci.ImageInfo, error) {
	if !RegexURL.MatchString(src) {
		log.Errorf("image url '%s' failed to parse regex check", src)
		return nil, ErrInvalidImage
	}
	ctx, cancel := context.WithTimeout(context.TODO(), time.Minute*5)
	defer cancel()
	return ins.GetLatest(ctx, src, regex)
}
