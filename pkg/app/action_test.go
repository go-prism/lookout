/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package app

import (
	"context"
	"errors"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/go-prism/lookout/pkg/oci"
	"gitlab.com/go-prism/lookout/pkg/output"
)

type testInspector struct{}

func (*testInspector) GetLatest(_ context.Context, src, _ string) (*oci.ImageInfo, error) {
	if !strings.HasPrefix(src, "docker://") {
		return nil, errors.New("expecting docker:// url scheme")
	}
	return &oci.ImageInfo{
		Source: src,
		Tag:    "latest",
		Sha:    "sha",
	}, nil
}

func TestAction_getLatest(t *testing.T) {
	a := NewAction()
	img, err := a.getLatest(&testInspector{}, "docker://docker.io/alpine", "")
	assert.NoError(t, err)
	assert.EqualValues(t, "latest", img.Tag)

	_, err = a.getLatest(&testInspector{}, "docker.io/alpine", "")
	assert.Error(t, err)
}

func TestAction_readInput(t *testing.T) {
	a := NewAction()
	img, err := a.readInput(&testInspector{}, "../../test/quay.yaml")
	assert.NoError(t, err)

	assert.ElementsMatch(t, []*output.FormatPair{
		{
			Name:   "dex",
			Tag:    "latest",
			SHA:    "sha",
			Source: "docker://quay.io/dexidp/dex",
			UseSHA: false,
		},
		{
			Name:   "origin-console",
			Tag:    "latest",
			SHA:    "sha",
			Source: "docker://quay.io/openshift/origin-console",
			UseSHA: true,
		},
	}, img)
}
