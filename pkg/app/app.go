/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package app

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
	cli2 "gitlab.com/go-prism/lookout/pkg/cli"
	"gitlab.com/go-prism/lookout/pkg/internal"
	"os"
)

const (
	FlagVerbose = "verbose"
	FlagOutput  = "output"
	FlagInput   = "input"
	FlagBackend = "backend"

	FlagModeFile = "file"
	FlagModeURL  = "url"

	FlagOutputKV = "kv"
	FlagOutputVV = "vv"
	FlagOutputTF = "tf"

	FlagBackendSkopeo = "skopeo"
)

// Run retrieves the cli.App configuration
func Run() *cli.App {
	act := NewAction()
	return &cli.App{
		Name:    "Lookout",
		Usage:   "Detects a newer version of an OCI image",
		Flags:   getGlobalFlags(),
		Version: fmt.Sprintf("%s-%s (%s)", internal.Version, internal.GitCommit, internal.BuildDate),
		Action:  act.F,
		Before: func(c *cli.Context) error {
			log.SetOutput(os.Stderr)
			// setup logging
			debug := c.Bool(FlagVerbose)
			if debug {
				log.SetLevel(log.DebugLevel)
			} else {
				log.SetLevel(log.InfoLevel)
			}
			return nil
		},
	}
}

// getGlobalFlags gets a list of flags available to all App commands
func getGlobalFlags() []cli.Flag {
	return []cli.Flag{
		&cli.BoolFlag{
			Name:  FlagVerbose,
			Value: false,
			Usage: "enable verbose logging",
		},
		&cli.GenericFlag{
			Name: FlagInput,
			Value: &cli2.EnumValue{
				Enum:    []string{FlagModeFile, FlagModeURL},
				Default: FlagModeURL,
			},
			Aliases: []string{"i"},
			Usage:   "input is a config file, or an individual image url (e.g. docker://docker.io/library/nginx)",
		},
		&cli.GenericFlag{
			Name: FlagBackend,
			Value: &cli2.EnumValue{
				Enum:    []string{FlagBackendSkopeo},
				Default: FlagBackendSkopeo,
			},
			Aliases: []string{"b"},
			Usage:   "backend used to lookup image information",
		},
		&cli.GenericFlag{
			Name: FlagOutput,
			Value: &cli2.EnumValue{
				Enum:    []string{FlagOutputKV, FlagOutputTF, FlagOutputVV},
				Default: FlagOutputKV,
			},
			Aliases: []string{"o"},
			Usage:   "output format",
		},
	}
}
