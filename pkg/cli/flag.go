/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package cli

import (
	"fmt"
	"strings"
)

var (
	// ErrUnknownValue is returned when the user gives an
	// option which is not part of our enum.
	ErrUnknownValue = func(given string, allowed []string) error {
		return fmt.Errorf("unknown value '%s', allowed values are %s", given, strings.Join(allowed, ", "))
	}
)

// EnumValue is a flag which allows a user
// to select a single option from a predefined list
//
// https://github.com/urfave/cli/issues/602#issuecomment-416000118
type EnumValue struct {
	Enum     []string
	Default  string
	selected string
}

func (e *EnumValue) Set(value string) error {
	for _, enum := range e.Enum {
		if enum == value {
			e.selected = value
			return nil
		}
	}
	return ErrUnknownValue(value, e.Enum)
}

func (e EnumValue) String() string {
	if e.selected == "" {
		return e.Default
	}
	return e.selected
}
