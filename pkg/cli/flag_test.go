/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package cli

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestEnumValue_Set(t *testing.T) {
	ev := &EnumValue{
		Enum:    []string{"A", "B"},
		Default: "A",
	}
	assert.NoError(t, ev.Set("A"))
	assert.EqualValues(t, "A", ev.selected)

	assert.EqualValues(t, ev.Set("C").Error(), ErrUnknownValue("C", ev.Enum).Error())
}

func TestEnumValue_String(t *testing.T) {
	ev := &EnumValue{
		Enum:    []string{"A", "B"},
		Default: "A",
	}
	assert.EqualValues(t, "", ev.selected)
	assert.EqualValues(t, "A", ev.String())
	assert.NoError(t, ev.Set("B"))
	assert.EqualValues(t, "B", ev.String())
}
