/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package config

import (
	"io/ioutil"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

type imageConfig struct {
	Name  string `yaml:"name"`
	URL   string `yaml:"url"`
	Regex string `yaml:"regex"`
	Sha   bool   `yaml:"sha"`
}

type FileConfig struct {
	Images []imageConfig `yaml:"images"`
}

// ReadFile attempts to read a FileConfig
// from a given file path.
func ReadFile(path string) (*FileConfig, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		log.WithError(err).Errorf("failed to read file: '%s'", path)
		return nil, err
	}
	var c FileConfig
	err = yaml.Unmarshal(data, &c)
	if err != nil {
		log.WithError(err).Error("failed to parse yaml")
		return nil, err
	}
	return &c, nil
}
