/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package config

import (
	_ "embed"
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestReadFile(t *testing.T) {
	var cases = []struct {
		name string
		src  string
		err  bool
	}{
		{
			"valid file is parsed correctly",
			"./testdata/valid.yaml",
			false,
		},
		{
			"invalid yaml file is not parsed",
			"./testdata/invalid.yaml",
			true,
		},
		{
			"missing file is not read",
			"./testdata/not-real.yaml",
			true,
		},
	}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			c, err := ReadFile(tt.src)
			if !tt.err {
				assert.NoError(t, err)
				assert.NotNil(t, c)
			} else {
				assert.Error(t, err)
				assert.Nil(t, c)
			}
		})
	}
}
