/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package oci

import (
	"context"
	"encoding/json"
	"fmt"
	"os/exec"
	"regexp"
	"sync"
	"time"

	"github.com/containers/skopeo/cmd/skopeo/inspect"
	log "github.com/sirupsen/logrus"
)

const (
	CommandName     = "skopeo"
	LabelOCICreated = "org.opencontainers.image.created"
)

type SkopeoInspector struct{}

func (i *SkopeoInspector) GetLatest(ctx context.Context, src, regex string) (*ImageInfo, error) {
	var rgx *regexp.Regexp
	if regex != "" {
		r, err := regexp.Compile(regex)
		if err != nil {
			log.WithError(err).WithContext(ctx).Errorf("failed to compile regex: '%s'", regex)
			return nil, err
		}
		rgx = r
	}
	log.WithContext(ctx).Debugf("inspecting root image")
	image, err := i.inspect(ctx, src, "")
	if err != nil {
		return nil, err
	}
	var latestDate *time.Time
	log.Infof("using root image with digest '%s'", image.Digest.String())
	maxSize := len(image.RepoTags)
	tagData := make(chan *inspect.Output, maxSize)
	var wg sync.WaitGroup
	var latest = image
	for idx := range image.RepoTags {
		t := image.RepoTags[idx]
		if rgx != nil && !rgx.MatchString(t) {
			log.WithContext(ctx).Debugf("skipping tag '%s' due to not meeting regex", t)
			continue
		}
		log.WithContext(ctx).Debugf("scanning tag %d of %d (%s)", idx+1, maxSize, t)
		wg.Add(1)
		go i.asyncInspect(ctx, src, t, tagData, &wg)
	}
	wg.Wait()
	close(tagData)
	log.Infof("all responders have responded, scanning results")
	for tag := range tagData {
		if tag == nil {
			log.Debugf("skipping nil tag")
			continue
		}
		date, err := i.getDate(tag)
		if err != nil {
			log.WithError(err).WithContext(ctx).Error("failed to resolve tag date")
			continue
		}
		// if we're the first, assume we're the newest
		if latestDate == nil {
			latest = tag
			latestDate = date
			continue
		}
		// if this tag is newer, use it instead
		if date.After(*latestDate) {
			latest = tag
			latestDate = date
			log.Debugf("found newer tag with timestamp: '%s' (%s)", date, tag.Tag)
		} else {
			log.Debugf("found older tag with timestamp: '%s' (%s)", date, tag.Tag)
		}
	}
	log.Infof("resolved latest tag to '%s' (%s)", latest.Tag, latest.Digest.String())
	return &ImageInfo{
		Source: src,
		Tag:    latest.Tag,
		Sha:    latest.Digest.String(),
	}, nil
}

func (*SkopeoInspector) getDate(output *inspect.Output) (*time.Time, error) {
	if output.Created.String() != "1980-01-01 00:00:01 +0000 UTC" {
		return output.Created, nil
	}
	log.Debugf("failed to find a non-zero 'Creation' timestamp, checking alternate locations")
	// check the OCI creation date
	t, err := time.Parse(time.RFC3339, output.Labels[LabelOCICreated])
	return &t, err
}

func (i *SkopeoInspector) asyncInspect(ctx context.Context, repo, tag string, res chan *inspect.Output, wg *sync.WaitGroup) {
	defer wg.Done()
	resp, _ := i.inspect(ctx, repo, tag)
	// skopeo sometimes doesn't return the tag
	// so we override it since we know it
	if resp != nil && resp.Tag == "" {
		resp.Tag = tag
	}
	res <- resp
}

func (i *SkopeoInspector) inspect(ctx context.Context, repo, tag string) (*inspect.Output, error) {
	var src string
	if tag == "" {
		src = repo
	} else {
		src = fmt.Sprintf("%s:%s", repo, tag)
	}
	resp, err := i.run(ctx, "inspect", src)
	if err != nil {
		return nil, err
	}
	return i.parseOutput(resp)
}

func (*SkopeoInspector) run(ctx context.Context, args ...string) ([]byte, error) {
	log.WithContext(ctx).Debugf("executing command: '%s'", args)
	cmd := exec.CommandContext(ctx, CommandName, args...)
	stdout, err := cmd.CombinedOutput()
	if err != nil {
		log.WithError(err).WithContext(ctx).Error("command exited with error")
		log.WithContext(ctx).Debugf("dumping response: '%s'", string(stdout))
		return nil, err
	}
	return stdout, nil
}

func (*SkopeoInspector) parseOutput(src []byte) (*inspect.Output, error) {
	var output inspect.Output
	err := json.Unmarshal(src, &output)
	if err != nil {
		log.WithError(err).Error("failed to read json")
	}
	return &output, nil
}
