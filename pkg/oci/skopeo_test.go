/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package oci

import (
	_ "embed"
	"github.com/araddon/dateparse"
	"github.com/containers/skopeo/cmd/skopeo/inspect"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

var _ Inspector = &SkopeoInspector{}

//go:embed testdata/inspect-config.json
var dataInspectConfig string

//go:embed testdata/inspect-fedora.json
var dataInspectFedora string

func TestSkopeoInspector_parseOutput(t *testing.T) {
	var cases = []struct {
		name     string
		src      string
		tagCount int
	}{
		{
			"example inspection is parsed correctly",
			dataInspectFedora,
			3,
		},
		{
			"invalid json returns an error",
			dataInspectConfig,
			0,
		},
	}
	si := &SkopeoInspector{}

	for _, tt := range cases {
		t.Run(tt.name, func(t *testing.T) {
			out, err := si.parseOutput([]byte(tt.src))
			assert.NoError(t, err)
			assert.EqualValues(t, tt.tagCount, len(out.RepoTags))
		})
	}
}

func TestSkopeoInspector_getDate(t *testing.T) {
	si := &SkopeoInspector{}
	d1, err := si.getDate(&inspect.Output{
		Created: &time.Time{},
	})
	assert.NoError(t, err)
	assert.EqualValues(t, time.Time{}.String(), d1.String())

	bpZero := dateparse.MustParse("1980-01-01 00:00:01 +0000 UTC")

	d3, err := si.getDate(&inspect.Output{
		Created: &bpZero,
		Labels: map[string]string{
			LabelOCICreated: "2021-05-26T10:26:10Z",
		},
	})
	assert.NoError(t, err)
	assert.EqualValues(t, "2021-05-26 10:26:10 +0000 UTC", d3.String())
}
