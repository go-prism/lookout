/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package output

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestKeyValue_Format(t *testing.T) {
	kv := &KeyValue{}
	val, err := kv.Format([]*FormatPair{
		{
			Name:   "nginx",
			Tag:    "latest",
			SHA:    "sha",
			Source: "docker://docker.io/nginx",
			UseSHA: false,
		},
		{
			Name:   "alpine",
			Tag:    "latest",
			SHA:    "sha",
			Source: "docker://docker.io/alpine",
			UseSHA: false,
		},
	})
	assert.NoError(t, err)

	assert.EqualValues(t, "nginx=docker.io/nginx:latest\nalpine=docker.io/alpine:latest", val)
}
