/*
 *    Copyright 2021 Django Cass
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 *
 */

package output

import (
	"fmt"
	"strings"
)

// Terraform formats tags in a way
// that can be passed to the terraform CLI
// in a *.tfvars file
type Terraform struct{}

func (*Terraform) Format(images []*FormatPair) (string, error) {
	var lines = make([]string, len(images))
	for i, v := range images {
		if v.UseSHA {
			lines[i] = fmt.Sprintf("image_tag_%s = \"%s@%s\"", v.Name, strings.TrimPrefix(v.Source, "docker://"), v.SHA)
		} else {
			lines[i] = fmt.Sprintf("image_tag_%s = \"%s:%s\"", v.Name, strings.TrimPrefix(v.Source, "docker://"), v.Tag)
		}
	}
	return strings.Join(lines, "\n"), nil
}
